# Bar Book

## Installation

| Application   | Version       |
|---------------|---------------|
| Ruby          | 2.6.5         |
| Node          | 12.4.0        |
| PostgreSQL    | 12.0          |

We also rely on [Docker Compose](https://docs.docker.com/compose/) for
deployments in production.

## Development

1. Fetch the code
2. `bundle install` and `yarn install`
3. Set up PostgreSQL database as per `config/application.rb` or use provided `start_development_database.sh` (requires Docker)
4. `bundle exec rails db:prepare`
5. `bundle exec rails s` should get you running on `http://localhost:3000`

## Production

1. Internally, we use Docker Compose with a `docker-compose.yml` similar to the following:

```
version: '3.7'

services:
  database:
    image: postgres:12-alpine
    environment:
      POSTGRES_PASSWORD: <database password>
      POSTGRES_USER: <database user>
      POSTGRES_DB: <database name>
    volumes:
      - database_data:/var/lib/postgresql/data

  app:
    image: <where to fetch the image from>
    environment:
      RAILS_MASTER_KEY: <your master key here>
    depends_on:
      - database
    ports:
      - "3000:3000"

volumes:
  database_data:
```

2. Make sure that the database credentials stored in Rails credentials match what you have in the config below.

You can also set up deployment the traditional way (using mina or Capistrano).
