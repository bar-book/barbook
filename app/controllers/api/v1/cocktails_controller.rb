# frozen_string_literal: true

module Api
  module V1
    class CocktailsController < ApiController
      expose(:cocktails) { Cocktail.includes(:category).all }
      expose(:cocktail, find_by: :uuid)

      def index; end

      def show; end
    end
  end
end
