# frozen_string_literal: true

ENV['RAILS_ENV'] ||= 'test'
require_relative '../config/environment'
require 'rails/test_help'
require 'minitest/reporters'
Minitest::Reporters.use! Minitest::Reporters::JUnitReporter.new("test/reports", true, single_file: true)

class ActiveSupport::TestCase
  include FactoryBot::Syntax::Methods
  parallelize(workers: :number_of_processors)
end
