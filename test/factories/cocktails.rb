# frozen_string_literal: true

FactoryBot.define do
  factory :cocktail do
    sequence(:name) { |n| "Cocktail ##{n}" }
    ingredients { "MyText" }
    glassware { "MyString" }
    technique { "MyText" }
    garnish { "MyString" }
    signature { false }
    youtube_link { "https://www.youtube.com/watch?v=dQw4w9WgXcQ" }
    menu { false }

    association :category
  end
end
