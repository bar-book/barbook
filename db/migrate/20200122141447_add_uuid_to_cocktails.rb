# frozen_string_literal: true

class AddUuidToCocktails < ActiveRecord::Migration[6.0]
  def change
    add_column :cocktails, :uuid, :uuid, default: "gen_random_uuid()", null: false
  end
end
